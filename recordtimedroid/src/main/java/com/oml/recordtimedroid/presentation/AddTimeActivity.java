package com.oml.recordtimedroid.presentation;

import android.content.Intent;
import android.os.Bundle;
import android.text.InputFilter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.oml.recordtimedroid.R;
import com.oml.recordtimedroid.model.InputFilterMinMax;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class AddTimeActivity extends AppCompatActivity {
    Button btnUpdateTime;
    Button btnPlusHours;
    Button btnPlusPlusHours;
    Button btnMinusHours;
    Button btnMinusMinusHours;
    Button btnResetHours;
    Button btnPlusMinutes;
    Button btnMinusMinutes;
    Button btnPlusPlusMinutes;
    Button btnMinusMinusMinutes;
    Button btnResetMinutes;
    Button btnPlusSeconds;
    Button btnMinusSeconds;
    Button btnPlusPlusSeconds;
    Button btnMinusMinusSeconds;
    Button btnResetSeconds;
    Button btnPlusMilliseconds;
    Button btnMinusMilliseconds;
    Button btnPlusPlusMilliseconds;
    Button btnMinusMinusMilliseconds;
    Button btnResetMilliseconds;
    EditText etHours;
    EditText etMinutes;
    EditText etSeconds;
    EditText etMilliseconds;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_time);

        SimpleDateFormat sdfHora = new SimpleDateFormat("HH", Locale.ROOT);
        Date dDate = new Date();
        etHours = (EditText) findViewById(R.id.etACTHours);
        etHours.setText(sdfHora.format(dDate));
        etHours.setFilters(new InputFilter[]{new InputFilterMinMax("0", "23")});

        SimpleDateFormat sdfMinutes = new SimpleDateFormat("mm", Locale.ROOT);
        etMinutes = (EditText) findViewById(R.id.etACTMinutes);
        etMinutes.setText(sdfMinutes.format(dDate));
        etMinutes.setFilters(new InputFilter[]{new InputFilterMinMax("0", "59")});

        SimpleDateFormat sdfSeconds = new SimpleDateFormat("ss", Locale.ROOT);
        etSeconds = (EditText) findViewById(R.id.etACTSeconds);
        etSeconds.setText(sdfSeconds.format(dDate));
        etSeconds.setFilters(new InputFilter[]{new InputFilterMinMax("0", "59")});

        SimpleDateFormat sdfMilliseconds = new SimpleDateFormat("SSS", Locale.ROOT);
        etMilliseconds = (EditText) findViewById(R.id.etACTMilliseconds);
        etMilliseconds.setText(sdfMilliseconds.format(dDate));
        etMilliseconds.setFilters(new InputFilter[]{new InputFilterMinMax("0", "999")});

        btnUpdateTime = (Button) findViewById(R.id.btnUpdateTime);

        btnUpdateTime.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view) {
                if (etHours.getText().toString().isEmpty() || etMinutes.getText().toString().isEmpty()
                        || etSeconds.getText().toString().isEmpty() || etMilliseconds.getText().toString().isEmpty()) {
                    Toast.makeText(AddTimeActivity.this, getString(R.string.empty_field_add_time_activity), Toast.LENGTH_LONG).show();
                    return;
                }
                AddTimeActivity.this.finishActivity(ScrollingActivity.MY_REQUEST_CODE_ADD_TIME_ACTIVITY);
                Intent intent = new Intent();
                String time = String.format(Locale.ROOT, "%02d:%02d:%02d.%03d",
                        Integer.parseInt(etHours.getText().toString()),
                        Integer.parseInt(etMinutes.getText().toString()),
                        Integer.parseInt(etSeconds.getText().toString()),
                        Integer.parseInt(etMilliseconds.getText().toString()));
                intent.putExtra(ScrollingActivity.TIME_TEXT, time);
                setResult(RESULT_OK, intent);
                finish();
            }
        });

        btnPlusHours = (Button) findViewById(R.id.btnPlusHours);

        btnPlusHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etHours.getText().toString()) < 23) {
                    Integer iNewHours = Integer.parseInt(etHours.getText().toString()) + 1;
                    etHours.setText(String.format(Locale.ROOT, "%02d", iNewHours));
                }
            }
        });

        btnPlusPlusHours = (Button) findViewById(R.id.btnPlusPlusHours);

        btnPlusPlusHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etHours.getText().toString()) < 13) {
                    Integer iNewHours = Integer.parseInt(etHours.getText().toString()) + 10;
                    etHours.setText(String.format(Locale.ROOT, "%02d", iNewHours));
                }
            }
        });

        btnMinusHours = (Button) findViewById(R.id.btnMinusHours);

        btnMinusHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etHours.getText().toString()) > 0) {
                    Integer iNewHours = Integer.parseInt(etHours.getText().toString()) - 1;
                    etHours.setText(String.format(Locale.ROOT, "%02d", iNewHours));
                }
            }
        });

        btnMinusMinusHours = (Button) findViewById(R.id.btnMinusMinusHours);

        btnMinusMinusHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etHours.getText().toString()) > 9) {
                    Integer iNewHours = Integer.parseInt(etHours.getText().toString()) - 10;
                    etHours.setText(String.format(Locale.ROOT, "%02d", iNewHours));
                }
            }
        });

        btnPlusMinutes = (Button) findViewById(R.id.btnPlusMinutes);

        btnPlusMinutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etMinutes.getText().toString()) < 59) {
                    Integer iNewMinutes = Integer.parseInt(etMinutes.getText().toString()) + 1;
                    etMinutes.setText(String.format(Locale.ROOT, "%02d", iNewMinutes));
                }
            }
        });

        btnPlusPlusMinutes = (Button) findViewById(R.id.btnPlusPlusMinutes);

        btnPlusPlusMinutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etMinutes.getText().toString()) < 49) {
                    Integer iNewMinutes = Integer.parseInt(etMinutes.getText().toString()) + 10;
                    etMinutes.setText(String.format(Locale.ROOT, "%02d", iNewMinutes));
                }
            }
        });

        btnMinusMinutes = (Button) findViewById(R.id.btnMinusMinutes);

        btnMinusMinutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etMinutes.getText().toString()) > 0) {
                    Integer iNewMinutes = Integer.parseInt(etMinutes.getText().toString()) - 1;
                    etMinutes.setText(String.format(Locale.ROOT, "%02d", iNewMinutes));
                }
            }
        });

        btnMinusMinusMinutes = (Button) findViewById(R.id.btnMinusMinusMinutes);

        btnMinusMinusMinutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etMinutes.getText().toString()) > 9) {
                    Integer iNewMinutes = Integer.parseInt(etMinutes.getText().toString()) - 10;
                    etMinutes.setText(String.format(Locale.ROOT, "%02d", iNewMinutes));
                }
            }
        });

        btnPlusSeconds = (Button) findViewById(R.id.btnPlusSeconds);

        btnPlusSeconds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etSeconds.getText().toString()) < 59) {
                    Integer iNewSeconds = Integer.parseInt(etSeconds.getText().toString()) + 1;
                    etSeconds.setText(String.format(Locale.ROOT, "%02d", iNewSeconds));
                }
            }
        });

        btnPlusPlusSeconds = (Button) findViewById(R.id.btnPlusPlusSeconds);

        btnPlusPlusSeconds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etSeconds.getText().toString()) < 49) {
                    Integer iNewSeconds = Integer.parseInt(etSeconds.getText().toString()) + 10;
                    etSeconds.setText(String.format(Locale.ROOT, "%02d", iNewSeconds));
                }
            }
        });

        btnMinusSeconds = (Button) findViewById(R.id.btnMinusSeconds);

        btnMinusSeconds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etSeconds.getText().toString()) > 0) {
                    Integer iNewSeconds = Integer.parseInt(etSeconds.getText().toString()) - 1;
                    etSeconds.setText(String.format(Locale.ROOT, "%02d", iNewSeconds));
                }
            }
        });

        btnMinusMinusSeconds = (Button) findViewById(R.id.btnMinusMinusSeconds);

        btnMinusMinusSeconds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etSeconds.getText().toString()) > 9) {
                    Integer iNewSeconds = Integer.parseInt(etSeconds.getText().toString()) - 10;
                    etSeconds.setText(String.format(Locale.ROOT, "%02d", iNewSeconds));
                }
            }
        });

        btnPlusMilliseconds = (Button) findViewById(R.id.btnPlusMilliseconds);

        btnPlusMilliseconds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etMilliseconds.getText().toString()) < 999) {
                    Integer iNewMilliseconds = Integer.parseInt(etMilliseconds.getText().toString()) + 1;
                    etMilliseconds.setText(String.format(Locale.ROOT, "%03d", iNewMilliseconds));
                }
            }
        });

        btnPlusPlusMilliseconds = (Button) findViewById(R.id.btnPlusPlusMilliseconds);

        btnPlusPlusMilliseconds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etMilliseconds.getText().toString()) < 990) {
                    Integer iNewMilliseconds = Integer.parseInt(etMilliseconds.getText().toString()) + 10;
                    etMilliseconds.setText(String.format(Locale.ROOT, "%03d", iNewMilliseconds));
                }
            }
        });

        btnMinusMilliseconds = (Button) findViewById(R.id.btnMinusMilliseconds);

        btnMinusMilliseconds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etMilliseconds.getText().toString()) > 0) {
                    Integer iNewMilliseconds = Integer.parseInt(etMilliseconds.getText().toString()) - 1;
                    etMilliseconds.setText(String.format(Locale.ROOT, "%03d", iNewMilliseconds));
                }
            }
        });

        btnMinusMinusMilliseconds = (Button) findViewById(R.id.btnMinusMinusMilliseconds);

        btnMinusMinusMilliseconds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (Integer.parseInt(etMilliseconds.getText().toString()) > 9) {
                    Integer iNewMilliseconds = Integer.parseInt(etMilliseconds.getText().toString()) - 10;
                    etMilliseconds.setText(String.format(Locale.ROOT, "%03d", iNewMilliseconds));
                }
            }
        });

        btnResetHours = (Button) findViewById(R.id.btnResetHours);

        btnResetHours.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etHours.setText("0");
            }
        });

        btnResetMinutes = (Button) findViewById(R.id.btnResetMinutes);

        btnResetMinutes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etMinutes.setText("0");
            }
        });

        btnResetSeconds = (Button) findViewById(R.id.btnResetSeconds);

        btnResetSeconds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etSeconds.setText("0");
            }
        });

        btnResetMilliseconds = (Button) findViewById(R.id.btnResetMilliseconds);

        btnResetMilliseconds.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etMilliseconds.setText("0");
            }
        });

    }
}