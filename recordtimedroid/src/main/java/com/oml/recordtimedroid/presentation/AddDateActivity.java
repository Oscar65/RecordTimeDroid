package com.oml.recordtimedroid.presentation;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ScrollView;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import com.oml.recordtimedroid.R;

public class AddDateActivity extends AppCompatActivity {
    DatePicker datePicker;
    Button btnUpdateDate;
    ScrollView svActivity_add_date;
    //LinearLayout lLActivityAddDateMain;

    /**
     * Begin a nestable scroll operation along the given axes.
     *
     * <p>A view starting a nested scroll promises to abide by the following contract:</p>
     *
     * <p>The view will call startNestedScroll upon initiating a scroll operation. In the case
     * of a touch scroll this corresponds to the initial {@link MotionEvent#ACTION_DOWN}.
     * In the case of touch scrolling the nested scroll will be terminated automatically in
     * the same manner as {@link ViewParent#requestDisallowInterceptTouchEvent(boolean)}.
     * In the event of programmatic scrolling the caller must explicitly call
     * {@link #stopNestedScroll()} to indicate the end of the nested scroll.</p>
     *
     * <p>If <code>startNestedScroll</code> returns true, a cooperative parent was found.
     * If it returns false the caller may ignore the rest of this contract until the next scroll.
     * Calling startNestedScroll while a nested scroll is already in progress will return true.</p>
     *
     * <p>At each incremental step of the scroll the caller should invoke
     * {@link #dispatchNestedPreScroll(int, int, int[], int[]) dispatchNestedPreScroll}
     * once it has calculated the requested scrolling delta. If it returns true the nested scrolling
     * parent at least partially consumed the scroll and the caller should adjust the amount it
     * scrolls by.</p>
     *
     * <p>After applying the remainder of the scroll delta the caller should invoke
     * {@link #dispatchNestedScroll(int, int, int, int, int[]) dispatchNestedScroll}, passing
     * both the delta consumed and the delta unconsumed. A nested scrolling parent may treat
     * these values differently. See {@link ViewParent#onNestedScroll(View, int, int, int, int)}.
     * </p>
     *
     * @param axes Flags consisting of a combination of {@link #SCROLL_AXIS_HORIZONTAL} and/or
     *             {@link #SCROLL_AXIS_VERTICAL}.
     * @return true if a cooperative parent was found and nested scrolling has been enabled for
     *         the current gesture.
     *
     * @see #stopNestedScroll()
     * @see #dispatchNestedPreScroll(int, int, int[], int[])
     * @see #dispatchNestedScroll(int, int, int, int, int[])
     */
    /*public boolean startNestedScroll(int axes) {
        //return scrollView.startNestedScroll(axes);
        return false;
    }*/

     //OML:No hace falta
    //@RequiresApi(api = Build.VERSION_CODES.M)
    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_date);

        datePicker = (DatePicker) findViewById(R.id.datePicker);
        btnUpdateDate = (Button) findViewById(R.id.btnUpdateDate);
        svActivity_add_date = (ScrollView) findViewById(R.id.svActivity_add_date);

        btnUpdateDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(AddDateActivity.this, AddTimeActivity.class), ScrollingActivity.MY_REQUEST_CODE_ADD_TIME_ACTIVITY);
            }
        });

        /*This code from https://stackoverflow.com/questions/42293925/how-to-scroll-datepicker-when-placed-in-a-scrollview does not work for me
        datePicker.setOnTouchListener((v, event) -> {
            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                //  Disallow the touch request for parent scroll on touch of datepicker view
                requestDisallowParentInterceptTouchEvent(v, true);
            } else if (event.getAction() == MotionEvent.ACTION_UP || event.getAction() == MotionEvent.ACTION_CANCEL) {
                // Re-allows parent events
                requestDisallowParentInterceptTouchEvent(v, false);
            }

            return false;
        });*/

        //But researching I found this solution on my LG Leon API 23
        svActivity_add_date.getViewTreeObserver().addOnScrollChangedListener(new ViewTreeObserver.OnScrollChangedListener() {

            @Override
            public void onScrollChanged() {
                requestDisallowParentInterceptTouchEvent(datePicker, true);
            }
        });

    }

    public String getCurrentDate(){
        // Month starts on 0
        return datePicker.getDayOfMonth() + "-" + (datePicker.getMonth() + 1) + "-" + datePicker.getYear();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == ScrollingActivity.MY_REQUEST_CODE_ADD_TIME_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                String timeText = data.getStringExtra(ScrollingActivity.TIME_TEXT);
                String dateTime = getCurrentDate() + " " + timeText;
                Intent intent = new Intent();
                intent.putExtra(ScrollingActivity.DATETIME_TEXT, dateTime);
                setResult(RESULT_OK, intent);
                finish();
            }
        }
    }

    private void requestDisallowParentInterceptTouchEvent(@NonNull View v, Boolean disallowIntercept) {
        while (v.getParent() != null && v.getParent() instanceof View) {
            if (v.getParent() instanceof ScrollView) {
                v.getParent().requestDisallowInterceptTouchEvent(disallowIntercept);
            }
            v = (View) v.getParent();
        }
    }

}