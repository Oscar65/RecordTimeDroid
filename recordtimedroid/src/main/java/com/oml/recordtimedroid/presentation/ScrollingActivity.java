package com.oml.recordtimedroid.presentation;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import com.google.android.material.appbar.AppBarLayout;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;
import com.oml.recordtimedroid.R;
import com.oml.recordtimedroid.use_cases.DaysHoursMinutesSecondsMilliseconds;
import com.oml.recordtimedroid.data.MainDB;
import com.oml.recordtimedroid.model.MyTextView;
import com.oml.recordtimedroid.model.RegisterTimes;
import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.widget.NestedScrollView;
import android.os.CountDownTimer;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.LinearLayout;
import android.widget.Toast;
import java.io.FileInputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.Locale;
import static android.view.ViewGroup.LayoutParams.MATCH_PARENT;
import static android.view.ViewGroup.LayoutParams.WRAP_CONTENT;
import static com.oml.recordtimedroid.data.MainDB.extractRegisterTimes;

public class ScrollingActivity extends AppCompatActivity {
    private int iTotalLines;
    private long lPreviousDate;
    private String fileName;
    private MyTextView mtvSumLapses;
    private long lTotal;
    private long lInitialDate;
    private LinearLayout csLL;
    private final Context myContext = this;
    private MainDB mainDB;
    private Toast mToastToShow;
    private String mToastToShowMessage;
    private SimpleDateFormat sdfDateFormat;
    public static final int MY_REQUEST_CODE_ADD_DATE_ACTIVITY = 1;
    public static final int MY_REQUEST_CODE_ADD_TIME_ACTIVITY = 2;
    public static final String DATETIME_TEXT = "DATETIME_TEXT";
    public static final String TIME_TEXT = "TIME_TEXT";
    private int iNumSelected;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scrolling);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        this.csLL = findViewById(R.id.cs_linear_layout);
        this.mtvSumLapses = findViewById(R.id.sumLapses);
        this.mainDB = new MainDB(this);
        this.mToastToShowMessage = "";
        this.sdfDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS", Locale.ROOT);
        this.fileName = "lines";

        // Minus one. A value to don't calculate any difference.
        readAllLines(-1);

        FloatingActionButton fabAddTime = findViewById(R.id.fabAddTime);
        fabAddTime.setOnClickListener(this::recordTime);

        FloatingActionButton fabDeleteSelectedTimes = findViewById(R.id.fabDeleteSelectedTimes);
        fabDeleteSelectedTimes.setOnClickListener(this::deleteSelectedTimes);
    }

    private void calculateDaysHoursMinutesSeconds(long dIni, long dFin,
                                                  DaysHoursMinutesSecondsMilliseconds value) {
        long diff = dFin - dIni;
        long secondsInMilli = 1000L;
        long minutesInMilli = secondsInMilli * 60L;
        long hoursInMilli = minutesInMilli * 60L;
        long daysInMilli = hoursInMilli * 24L;

        long elapsedDays = diff / daysInMilli;
        diff = diff % daysInMilli;

        long elapsedHours = diff / hoursInMilli;
        diff = diff % hoursInMilli;

        long elapsedMinutes = diff / minutesInMilli;
        diff = diff % minutesInMilli;

        long elapsedSeconds = diff / secondsInMilli;
        diff = diff % secondsInMilli;

        value.setDays(elapsedDays);
        value.setHours(elapsedHours);
        value.setMinutes(elapsedMinutes);
        value.setSeconds(elapsedSeconds);
        value.setMilliseconds(diff);
    }

    @Override
    public boolean onCreateOptionsMenu(@NonNull Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_scrolling, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == R.id.action_reset) {
            try {
                mainDB.resetDB();
                int csLLGCC = csLL.getChildCount();
                for (int i = 1; i < csLLGCC; i++) {
                    csLL.removeView(csLL.getChildAt(1));
                }

                iTotalLines = 0;
                mtvSumLapses.setText("");
                mtvSumLapses.setlDiference(0);
                lInitialDate = -1;

            } catch(Exception e) {
                e.printStackTrace();
                //Toast.makeText(getBaseContext(),e.getMessage(), Toast.LENGTH_LONG).show();
                showToast(getBaseContext(), e.getMessage(), 10000);
            }

            return true;
        }

        if (id == R.id.action_delete_first) {
            for (int i = 1; i < csLL.getChildCount(); i++) {
                if (csLL.getChildAt(i) instanceof MyTextView) {

                    mainDB.delete(((MyTextView) csLL.getChildAt(i)).get_id());

                    calculateTotal1(i);

                    csLL.removeViewAt(i);
                    iTotalLines--;

                    if ( i < csLL.getChildCount()) {
                        int _id = ((MyTextView) csLL.getChildAt(i)).get_id();
                        RegisterTimes rt = mainDB.element(_id);
                        lInitialDate = rt.getTime();
                    } else {
                        lInitialDate = -1;
                    }

                    if (i < csLL.getChildCount()) {
                        MyTextView mtv = (MyTextView) csLL.getChildAt(i);
                        String s = mtv.getText().toString();
                        String[] sa = s.split("\\|");
                        String text = sa[0] + "|" + sa[1] + "|";
                        mtv.setText(text);
                        if (mtv.isSelected())
                            mtv.callOnClick();
                        mtv.setOnClickListener(null);
                    }

                    break;
                }
            }

            calculateMtvSumLapses(0);

            return true;
        }

        if (id == R.id.action_delete_selected) {
            deleteSelectedTimes(null);

            return true;
        }

        if (id == R.id.action_go_top) {
            AppBarLayout abl = findViewById(R.id.app_bar);
            abl.setExpanded(true);
            NestedScrollView nsv = findViewById(R.id.content_scrolling);
            nsv.fullScroll(View.FOCUS_UP);

            return true;
        }

        if (id == R.id.action_go_bottom) {
            AppBarLayout abl = findViewById(R.id.app_bar);
            abl.setExpanded(false);
            NestedScrollView nsv = findViewById(R.id.content_scrolling);
            nsv.fullScroll(View.FOCUS_DOWN);

            return true;
        }

        if (id == R.id.action_record_time) {
            View view = findViewById(R.id.content_scrolling);
            recordTime(view);

            return true;
        }

        if (id == R.id.action_add_date) {
            startActivityForResult(new Intent(this, AddDateActivity.class), MY_REQUEST_CODE_ADD_DATE_ACTIVITY);

            return true;
        }


        if (id == R.id.action_show_timezone) {
            this.sdfDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS z", Locale.ROOT);
            deleteAllMyTextViewExceptSumLapses();
            readAllLines(-1);

            return true;
        }

        if (id == R.id.action_hide_timezone) {
            this.sdfDateFormat = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS", Locale.ROOT);
            deleteAllMyTextViewExceptSumLapses();
            readAllLines(-1);

            return true;
        }

        if (id == R.id.action_about) {
            Intent i = new Intent(this, AboutActivity.class);
            startActivity(i);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    //On click listener for dynamic myTextView
    final private View.OnClickListener mtv_OnClickListener = new View.OnClickListener() {
        public void onClick(final View v) {
            if (((MyTextView) v).getSelectionStart() != ((MyTextView) v).getSelectionEnd()) return;
            // Issue:
            // If you click over a selected line it doesn't deselect if you click again.
            // You need to select another line and click again the previous line to deselect it.
            // Executes twice this method, so I set enabled to false to avoid it.
            v.setEnabled(false);
            boolean selected = !v.isSelected();
            v.setSelected(selected);
            v.setBackgroundColor(selected ? Color.CYAN : Color.TRANSPARENT);

            RegisterTimes rt = mainDB.element(((MyTextView) v).get_id());
            if (selected != rt.getSelected()) {
                rt.setSelected(selected);
                mainDB.update(rt.get_id(), rt);
            }

            if (selected) {
                iNumSelected++;
                calculateMtvSumLapses(((MyTextView) v).getlDiferencia());
            } else {
                iNumSelected--;
                calculateMtvSumLapses(-((MyTextView) v).getlDiferencia());
            }

            v.setEnabled(true);
        }
    };

    private void addDateDB(RegisterTimes r) {
        mainDB.add(r);
    }

    private void showToast(Context context, String message, int toastDurationInMilliSeconds) {
        if (mToastToShowMessage.equals("")) {
            mToastToShowMessage = message;
        } else {
            mToastToShowMessage += "\n" + message;
        }
        mToastToShow = Toast.makeText(context, mToastToShowMessage, Toast.LENGTH_LONG);

        // Set the countdown to display the toast
        CountDownTimer toastCountDown;
        toastCountDown = new CountDownTimer(toastDurationInMilliSeconds, 1000) {
            public void onTick(long millisUntilFinished) {
                mToastToShow.show();
            }

            public void onFinish() {
                mToastToShow.cancel();
                mToastToShowMessage = "";
            }
        };

        // Show the toast and starts the countdown
        mToastToShow.show();
        toastCountDown.start();
    }

    private void readAllLines(long lMilliseconds) {
        runOnUiThread(() -> {
            DaysHoursMinutesSecondsMilliseconds dhmsmTempo =
                    new DaysHoursMinutesSecondsMilliseconds();
            StringBuilder sbBuff = new StringBuilder();

            try {
                lInitialDate = -1;
                iTotalLines = 0;
                iNumSelected = 0;
                mtvSumLapses.setlDiference(0);

                String[] sFileList = myContext.fileList();
                // Read data from sequential file, saves it in DB SQLite and deletes file.
                if (Arrays.asList(sFileList).contains(fileName)) {
                    FileInputStream fin = openFileInput(fileName);
                    int c;
                    long lDate;
                    lPreviousDate = -1;

                    mtvSumLapses.setlDiference(0);

                    while ((c = fin.read()) != -1) {
                        if (c == '\n') {
                            // To be backward compatible with file format dd-MM-yyyy HH:mm:ss.SSS
                            if (sbBuff.toString().contains("-")) {
                                Date d = sdfDateFormat.parse(sbBuff.toString());
                                if (d != null) {
                                    lDate = d.getTime();
                                    //Add Date to DB
                                    RegisterTimes r = new RegisterTimes(lDate);
                                    addDateDB(r);
                                } else {
                                    lDate = 0;
                                }
                            } else {
                                lDate = Long.parseLong(sbBuff.toString());
                                // Add Date to DB
                                RegisterTimes reg = new RegisterTimes(lDate);
                                addDateDB(reg);
                            }
                            if (lInitialDate == -1)
                                lInitialDate = lDate;
                            lPreviousDate = lDate;
                            sbBuff.setLength(0);
                            lTotal = lInitialDate - lDate;
                        } else {
                            sbBuff.append( (char)c );
                        }
                    }
                    fin.close();
                    // Delete file
                    boolean ret = deleteFile(fileName);
                    if (!ret) {
                        showToast(getBaseContext(),
                                getResources().getString(R.string.error_deleting_file) +
                                " " + fileName, 10000);
                    }
                }

                // TODO:Read data from SQLite DB
                // SELECT _id, DATETIME(ROUND(time / 1000), 'unixepoch') AS isodate FROM recordtimes;
                // INSERT INTO recordtimes (time) VALUES(1589025299803);
                // DELETE FROM recordtimes WHERE _id = 282;
                Cursor c = mainDB.extractCursor();
                lPreviousDate = -1;
                RegisterTimes registerTimes;
                String sDifferenceAddedTime = null;
                int idMilliseconds = -1;

                while (c.moveToNext()) {
                    registerTimes = extractRegisterTimes(c);
                    if (lInitialDate == -1)
                        lInitialDate = registerTimes.getTime();
                    Date d = new Date(registerTimes.getTime());
                    String sDate = sdfDateFormat.format(d);
                    // Create MyTextView dynamically
                    MyTextView mtv = new MyTextView(myContext);
                    mtv.setText(String.format(Locale.ROOT, "%03d | %s |",
                            registerTimes.get_id(), sDate));
                    mtv.setSelected(false);
                    if (registerTimes.getTime() == lMilliseconds)
                        idMilliseconds = registerTimes.get_id();
                    if (lPreviousDate != -1) {
                        calculateDaysHoursMinutesSeconds(lPreviousDate,
                                registerTimes.getTime(), dhmsmTempo);
                        String sDifference = String.format(Locale.ROOT,
                                "%03d %s %02d:%02d:%02d.%03d",
                                (int) dhmsmTempo.getDays(),
                                getResources().getString(R.string.days),
                                (int) dhmsmTempo.getHours(),
                                (int) dhmsmTempo.getMinutes(),
                                (int) dhmsmTempo.getSeconds(),
                                (int) dhmsmTempo.getMilliseconds());
                        if (lPreviousDate == lMilliseconds || registerTimes.getTime() == lMilliseconds)
                            sDifferenceAddedTime = " id=" + idMilliseconds + " +" + sDifference;
                        mtv.setText(String.format(Locale.ROOT,
                                "%s %03d %s %02d:%02d:%02d.%03d",
                                mtv.getText(),
                                (int) dhmsmTempo.getDays(),
                                getResources().getString(R.string.days),
                                (int) dhmsmTempo.getHours(),
                                (int) dhmsmTempo.getMinutes(),
                                (int) dhmsmTempo.getSeconds(),
                                (int) dhmsmTempo.getMilliseconds()));
                        mtv.setlDiference(registerTimes.getTime() - lPreviousDate);
                        mtv.setOnClickListener(mtv_OnClickListener);
                    }

                    lPreviousDate = registerTimes.getTime();
                    sbBuff.setLength(0);
                    mtv.setTextIsSelectable(true);
                    mtv.set_id(registerTimes.get_id());
                    mtv.resizeText();
                    csLL.addView(mtv);

                    if (registerTimes.getSelected())
                        mtv.callOnClick();

                    iTotalLines++;
                }

                lTotal = lPreviousDate - lInitialDate;

                calculateMtvSumLapses(0);

                c.close();

                if (sDifferenceAddedTime != null) {
                    String notification = getResources().getString(R.string.notification) + " " +
                            iTotalLines + sDifferenceAddedTime;
                    Snackbar.make(csLL, notification, Snackbar.LENGTH_LONG)
                            .setAction("Action", null).show();
                }

            } catch(Exception e) {
                e.printStackTrace();
                showToast(getBaseContext(), e.getMessage(), 10000);
            }
        });
    }

    private void subtractTotalAndSelected(int pos) {
        if ( csLL.getChildAt(pos) instanceof MyTextView ) {
            MyTextView mtv = (MyTextView) csLL.getChildAt( pos );
            if (pos == csLL.getChildCount() - 1)
                lTotal -= mtv.getlDiferencia();
            if (mtv.isSelected()) {
                mtv.callOnClick();
            }
        }
    }

    private void reCalculateDifference(int pos) {
        MyTextView mtv = (MyTextView) csLL.getChildAt(pos + 1);

        int _id1 = ((MyTextView) csLL.getChildAt(pos)).get_id();
        RegisterTimes rt1 = mainDB.element(_id1);

        int _id2 = ((MyTextView) csLL.getChildAt(pos + 1)).get_id();
        RegisterTimes rt2 = mainDB.element(_id2);

        mtv.setlDiference(rt2.getTime() - rt1.getTime());

        DaysHoursMinutesSecondsMilliseconds dhmsmTempo =
                new DaysHoursMinutesSecondsMilliseconds();

        Date d = new Date(rt2.getTime());
        String sDate = sdfDateFormat.format(d);
        mtv.setText(String.format(Locale.ROOT, "%03d | %s |", rt2.get_id(), sDate));
        calculateDaysHoursMinutesSeconds(rt1.getTime(), rt2.getTime(), dhmsmTempo);
        mtv.setText(String.format(Locale.ROOT, "%s %03d %s %02d:%02d:%02d.%03d",
            mtv.getText(),
            (int) dhmsmTempo.getDays(), getResources().getString(R.string.days),
            (int) dhmsmTempo.getHours(),
            (int) dhmsmTempo.getMinutes(), (int) dhmsmTempo.getSeconds(),
            (int) dhmsmTempo.getMilliseconds()));
    }

    private void calculateTotal1(int pos) {
        if ( (pos + 1) < csLL.getChildCount() ) {
            lTotal -= ((MyTextView) csLL.getChildAt(pos + 1)).getlDiferencia();
        } else {
            lTotal = 0;
        }

        boolean bSubTotal = false;

        for (int i = 1; i < csLL.getChildCount(); i++) {

            if ( csLL.getChildAt( i ) instanceof MyTextView ) {
                MyTextView mtv = (MyTextView) csLL.getChildAt(i);

                if (mtv.isSelected()) {
                    bSubTotal = true;
                    break;
                }

            }
        }

        if (bSubTotal) {
            DaysHoursMinutesSecondsMilliseconds dhmsmTempo =
                    new DaysHoursMinutesSecondsMilliseconds();
            calculateDaysHoursMinutesSeconds(0, mtvSumLapses.getlDiferencia(), dhmsmTempo);
            String sText;
            if (iNumSelected == 1)
                sText = getResources().getString(R.string.selected_lines_singular);
            else
                sText = getResources().getString(R.string.selected_lines_plural);
                mtvSumLapses.setText(String.format(Locale.ROOT, "%s %03d %s %02d:%02d:%02d.%03d",
                    sText,
                    (int) dhmsmTempo.getDays(),
                    getResources().getString(R.string.days),
                    (int) dhmsmTempo.getHours(),
                    (int) dhmsmTempo.getMinutes(),
                    (int) dhmsmTempo.getSeconds(),
                    (int) dhmsmTempo.getMilliseconds()));
        } else {
            if (lTotal == 0L) {
                mtvSumLapses.setText("");
            } else {
                DaysHoursMinutesSecondsMilliseconds dhmsmTempo =
                        new DaysHoursMinutesSecondsMilliseconds();
                calculateDaysHoursMinutesSeconds(0, lTotal, dhmsmTempo);
                mtvSumLapses.setText(String.format(Locale.ROOT,
                        "%s %03d %s %02d:%02d:%02d.%03d",
                        getResources().getString(R.string.total),
                        (int) dhmsmTempo.getDays(),
                        getResources().getString(R.string.days),
                        (int) dhmsmTempo.getHours(),
                        (int) dhmsmTempo.getMinutes(),
                        (int) dhmsmTempo.getSeconds(),
                        (int) dhmsmTempo.getMilliseconds()));
            }
        }
    }

    @Override
    public void onDestroy(){
        super.onDestroy();
        mainDB.close();
    }

    private void recordTime(View view) {
        Date dDateHour = new Date();
        long lActualDate = dDateHour.getTime();

        if (lInitialDate == -1) lInitialDate = lActualDate;
        String sDate = sdfDateFormat.format(dDateHour);
        MyTextView mtv = new MyTextView(myContext);

        // Save time in DB with unix timestamp in milliseconds
        int _id = -1;
        try {
            RegisterTimes registerTimes = new RegisterTimes(lActualDate);
            _id = mainDB.add(registerTimes);
            mtv.set_id(_id);
        } catch(Exception e) {
            e.printStackTrace();
            showToast(getBaseContext(), e.getMessage(), 10000);
        }

        LinearLayout.LayoutParams llLP = new LinearLayout.LayoutParams(MATCH_PARENT,
                WRAP_CONTENT);
        llLP.setMargins(0, 2, 0, 0);
        mtv.setLayoutParams(llLP);
        mtv.setText(String.format(Locale.ROOT, "%03d | %s |", _id, sDate));
        if (iTotalLines > 0) {
            DaysHoursMinutesSecondsMilliseconds dhmsmTempo =
                    new DaysHoursMinutesSecondsMilliseconds();
            calculateDaysHoursMinutesSeconds(lPreviousDate, lActualDate, dhmsmTempo);
            String sDifference = String.format(Locale.ROOT,
                    "%03d %s %02d:%02d:%02d.%03d",
                    (int) dhmsmTempo.getDays(),
                    getResources().getString(R.string.days),
                    (int) dhmsmTempo.getHours(),
                    (int) dhmsmTempo.getMinutes(),
                    (int) dhmsmTempo.getSeconds(),
                    (int) dhmsmTempo.getMilliseconds());
            mtv.setText(String.format(Locale.ROOT, "%s %s", mtv.getText(),
                    sDifference));
            mtv.resizeText();
            lTotal = lInitialDate - lActualDate;
            mtv.setlDiference(lPreviousDate - lActualDate);

            boolean bSubTotal = false;

            for (int i = 0; i < csLL.getChildCount(); i++) {
                if (csLL.getChildAt(i).isSelected()) {
                    bSubTotal = true;
                    break;
                }
            }

            if (!bSubTotal) {

                if (lTotal == 0L) {
                    mtvSumLapses.setText("");
                } else {
                    calculateDaysHoursMinutesSeconds(lTotal, 0, dhmsmTempo);
                    mtvSumLapses.setText(String.format(Locale.ROOT,
                            "%s %03d %s %02d:%02d:%02d.%03d",
                            getResources().getString(R.string.total),
                            (int) dhmsmTempo.getDays(),
                            getResources().getString(R.string.days),
                            (int) dhmsmTempo.getHours(),
                            (int) dhmsmTempo.getMinutes(),
                            (int) dhmsmTempo.getSeconds(),
                            (int) dhmsmTempo.getMilliseconds()));
                    mtvSumLapses.resizeText();
                }
            }

            mtv.setOnClickListener(mtv_OnClickListener);
            iTotalLines++;
            if (!sDifference.contains("-")) {
                String notification = getResources().getString(R.string.notification) + " " +
                        iTotalLines + " +" + sDifference;
                Snackbar.make(view, notification, Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        }

        lPreviousDate = lActualDate;
        mtv.setTextIsSelectable(true);
        csLL.addView(mtv);

        if (lTotal < lInitialDate) {
            deleteAllMyTextViewExceptSumLapses();
            readAllLines(lActualDate);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == MY_REQUEST_CODE_ADD_DATE_ACTIVITY) {
            if (resultCode == RESULT_OK) {
                String dateTimeReturned = data.getStringExtra(DATETIME_TEXT);
                SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss.SSS", Locale.ROOT);
                long milliseconds = 0L;
                try {
                    Date d = sdf.parse(dateTimeReturned);
                    milliseconds = d.getTime();
                    RegisterTimes rt = new RegisterTimes(milliseconds);
                    addDateDB(rt);
                    deleteAllMyTextViewExceptSumLapses();
                    readAllLines(milliseconds);
                } catch (Exception e) {
                    showToast(myContext, e.getMessage(), 5000);
                    e.printStackTrace();
                }
            }
        }
    }

    private void deleteAllMyTextViewExceptSumLapses() {
        while (csLL.getChildCount() > 1) {
            View v = csLL.getChildAt(1);
            csLL.removeView(v);
        }
    }

    private void calculateMtvSumLapses(long lDifference) {
        DaysHoursMinutesSecondsMilliseconds dhmsmTempo =
                new DaysHoursMinutesSecondsMilliseconds();

        mtvSumLapses.setlDiference(mtvSumLapses.getlDiferencia() + lDifference);

        if (iNumSelected == 0 && lTotal == 0) {
            mtvSumLapses.setText("");
        } else if (iNumSelected == 0) {
            calculateDaysHoursMinutesSeconds(0, lTotal, dhmsmTempo);
            mtvSumLapses.setText(String.format(Locale.ROOT,
                    "%s %d %s %03d %s %02d:%02d:%02d.%03d",
                    getResources().getString(R.string.total),
                    iTotalLines,
                    getResources().getString(R.string.lines),
                    (int) dhmsmTempo.getDays(),
                    getResources().getString(R.string.days),
                    (int) dhmsmTempo.getHours(),
                    (int) dhmsmTempo.getMinutes(),
                    (int) dhmsmTempo.getSeconds(),
                    (int) dhmsmTempo.getMilliseconds()));
        } else {
            calculateDaysHoursMinutesSeconds(0, mtvSumLapses.getlDiferencia(), dhmsmTempo);
            String sText;

            if (iNumSelected == 1)
                sText = getResources().getString(R.string.selected_lines_singular);
            else
                sText = getResources().getString(R.string.selected_lines_plural);
                mtvSumLapses.setText(String.format(Locale.ROOT,
                    "%d/%d %s %03d %s %02d:%02d:%02d.%03d",
                    iNumSelected,
                    iTotalLines,
                    sText,
                    (int) dhmsmTempo.getDays(),
                    getResources().getString(R.string.days),
                    (int) dhmsmTempo.getHours(),
                    (int) dhmsmTempo.getMinutes(),
                    (int) dhmsmTempo.getSeconds(),
                    (int) dhmsmTempo.getMilliseconds()));
        }

        mtvSumLapses.resizeText();
    }

    private void deleteSelectedTimes(View view) {
        boolean change_lPreviousDate = false;
        boolean changeMtvNext = false;

        for (int i = csLL.getChildCount() - 1; i > 0; i--) {
            if (csLL.getChildAt( i ) instanceof MyTextView) {

                MyTextView mtv = (MyTextView) csLL.getChildAt( i );

                if ( changeMtvNext && (i < (csLL.getChildCount() - 1)) ) {
                    reCalculateDifference(i);
                }

                if (change_lPreviousDate) {
                    int _id = mtv.get_id();
                    if (_id == -1) {
                        lPreviousDate = -1;
                    } else {
                        RegisterTimes rt = mainDB.element(_id);
                        lPreviousDate = rt.getTime();
                    }
                    change_lPreviousDate = false;
                }

                if (mtv.isSelected()) {
                    if (i == (csLL.getChildCount() - 1))
                        change_lPreviousDate = true;
                    subtractTotalAndSelected(i);
                    mainDB.delete(mtv.get_id());
                    csLL.removeViewAt(i);
                    iTotalLines--;
                    changeMtvNext = true;
                }
            }
        }

        if (!changeMtvNext)
            showToast(myContext, getString(R.string.no_lines_selected), 5000);

        calculateMtvSumLapses(0);
    }

}