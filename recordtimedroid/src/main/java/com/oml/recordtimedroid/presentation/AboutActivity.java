package com.oml.recordtimedroid.presentation;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.DateFormat;
import android.view.WindowManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import com.oml.recordtimedroid.R;
import com.oml.recordtimedroid.BuildConfig;

import java.util.Calendar;
import java.util.Locale;

public class AboutActivity extends Activity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_html);

        WebView wv = findViewById(R.id.wvAbout);
        wv.setWebViewClient(new WebViewClient());
        // Set the Text Size to fit text on window
        wv.getSettings().setTextZoom(80);
        wv.getSettings().setUseWideViewPort(true);
        wv.getSettings().setLoadWithOverviewMode(true);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(BuildConfig.VERSION_BUILD_DATE);
        String html = getResources().getString(R.string.message_about_html,
                BuildConfig.VERSION_NAME, BuildConfig.VERSION_BUILD, calendar.getTime().toLocaleString());
        wv.loadData(html, "text/html", "UTF-8");

        /*
         *
         * Open external browser. WebView does not work with icons8.com.
         * And gitlab.com needs JavaScript.
         *
         */
        wv.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(final WebView view, final String url) {
                Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(url));
                startActivity(i);
                return true;
            }
        });

    }

}
