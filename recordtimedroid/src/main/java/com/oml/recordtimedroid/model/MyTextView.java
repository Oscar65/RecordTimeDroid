package com.oml.recordtimedroid.model;

import android.content.Context;
import android.os.Build;
import android.util.AttributeSet;
import android.util.TypedValue;
import android.view.MotionEvent;

// Issue:
//         User needs to click twice to select line.
//
// Solution:
//            https://android-er.blogspot.com/2014/09/warning-custom-view-overrides.html

public class MyTextView extends androidx.appcompat.widget.AppCompatTextView {
    private long lDiferencia;
    boolean touchOn;
    boolean mDownTouch = false;
    int _id;
    Context context;

    public MyTextView(Context context)
    {
        super(context);
        this.context = context;

        init();
    }

    public MyTextView(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    public MyTextView(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
    }

    public long getlDiferencia() {
        return lDiferencia;
    }

    public void setlDiference(long lDiferencia) {
        this.lDiferencia = lDiferencia;
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);

        // Listening for the down and up touch events
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:

                touchOn = !touchOn;
                invalidate();

                mDownTouch = true;
                return true;

            case MotionEvent.ACTION_UP:
                if (mDownTouch) {
                    mDownTouch = false;
                    performClick(); // Call this method to handle the response, and
                    // thereby enable accessibility services to
                    // perform this action for a user who cannot
                    // click the touchscreen.
                    return true;
                }
        }

        return false; // Return false for other touch events
    }

    @Override
    public boolean performClick() {
        // Calls the super implementation, which generates an AccessibilityEvent
        // and calls the onClick() listener on the view, if any
        super.performClick();

        // Handle the action for the custom click here

        return true;
    }

    private void init() {
        this.lDiferencia = 0;
        this._id = -1;
        touchOn = false;
    }

    public int get_id() { return _id; }

    public void set_id(int _id) { this._id = _id; }

    public void resizeText() {
        // Get the screen's density scale
        final float density_dp = getResources().getDisplayMetrics().density;
        final float density_dpi = getResources().getDisplayMetrics().densityDpi;
        final float scaledDensity_sp = getResources().getDisplayMetrics().scaledDensity;
        final float heightPixels = getResources().getDisplayMetrics().heightPixels;
        final float widthPixels = getResources().getDisplayMetrics().widthPixels;
        // Convert the dps to pixels, based on density scale
        float sp = ((widthPixels / scaledDensity_sp) / 53) * 2;

        // For EMUI 12.0.0
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q)
            sp--;

        this.setTextSize(TypedValue.COMPLEX_UNIT_SP, sp);
    }

}
