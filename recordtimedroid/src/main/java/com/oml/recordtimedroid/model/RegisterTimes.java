package com.oml.recordtimedroid.model;

public class RegisterTimes {
    private int _id;
    private long time;
    private int isSelected;

    public RegisterTimes(long time) {
        this.time = time;
    }

    public RegisterTimes() {
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int get_id() {
        return _id;
    }

    public void set_id(int _id) {
        this._id = _id;
    }

    public boolean getSelected() {
        return this.isSelected == 1;
    }

    public void setSelected(boolean selected) {
        if (selected)
            this.isSelected = 1;
        else
            this.isSelected = 0;
    }

}
