package com.oml.recordtimedroid.data;

import com.oml.recordtimedroid.model.RegisterTimes;

public interface RepositoryTimes {
        // Return element id
        RegisterTimes element(int id);
        // Add element indicated
        int add(RegisterTimes r);
        // Delete element with id indicated
        void delete(int id);
        // Return size
        int size();
        // Update element
        void update(int id, RegisterTimes r);
        // Delete all data
        void resetDB();
    }
