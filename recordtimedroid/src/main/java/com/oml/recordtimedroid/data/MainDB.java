package com.oml.recordtimedroid.data;

import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.oml.recordtimedroid.R;
import com.oml.recordtimedroid.model.RegisterTimes;

public class MainDB extends SQLiteOpenHelper implements RepositoryTimes {

    Context context;

    public MainDB(Context context) {
        super(context, "recordtimedroid.sqlite", null ,2);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase bd) {
        bd.execSQL("CREATE TABLE recordtimes ("+
                "_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                "time LONG," +
                "is_selected NOT NULL CHECK (is_selected IN (0, 1)) DEFAULT 0)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,
                                             int newVersion) {
        if (newVersion > oldVersion)
            db.execSQL("ALTER TABLE recordtimes ADD COLUMN is_selected NOT NULL CHECK (is_selected IN (0, 1)) DEFAULT 0");
    }

    public static RegisterTimes extractRegisterTimes(Cursor cursor) {
        RegisterTimes registerTime = new RegisterTimes();
        registerTime.set_id(cursor.getInt(0));
        registerTime.setTime(cursor.getLong(1));
        registerTime.setSelected(cursor.getInt(2) == 1);
        return registerTime;
    }

    public Cursor extractCursor() {
        String order = "time, _id";
        String query = "SELECT * FROM recordtimes ORDER BY " +
                order;
        SQLiteDatabase bd = getReadableDatabase();
        return bd.rawQuery(query, null);
    }

    @Override
    public void update(int id, RegisterTimes registerTimes) {
        String sql = "UPDATE recordtimes SET " +
                " time = " +
                registerTimes.getTime() +
                ", is_selected = " + (registerTimes.getSelected() ? 1 : 0) +
                " WHERE _id = " + id;
        getWritableDatabase().execSQL(sql);
    }

    @Override
    public int size() {
        return 0;
    }

    @Override
    public void delete(int id) {
        getWritableDatabase().execSQL("DELETE FROM recordtimes WHERE _id = " + id);
    }

    @Override
    public int add(RegisterTimes registerTimes) {
        int _id = 0;

        getWritableDatabase().execSQL("INSERT INTO recordtimes (time) VALUES (" +
                registerTimes.getTime() + ")");
        Cursor c = getReadableDatabase().rawQuery(
                "SELECT _id FROM recordtimes WHERE time = " + registerTimes.getTime(), null);
        if (c.moveToNext()) _id = c.getInt(0);
        c.close();

        return _id;
    }

    @Override
    public RegisterTimes element(int id) {
        try (Cursor cursor = getReadableDatabase().rawQuery(
                "SELECT * FROM recordtimes WHERE _id = " + id, null)) {
            if (cursor.moveToNext())
                return extractRegisterTimes(cursor);
            else
                throw new SQLException(context.getResources().getString(R.string.error_accessing_element) + " " + id);
        } catch (Exception e) {
            Log.i("ERROR", "element: Exception: " + e.getMessage());
            throw e;
        }
    }

    @Override
    public void resetDB() {
        this.close();
        String dbPath = context.getDatabasePath(this.getDatabaseName()).getPath();
        context.deleteDatabase(this.getDatabaseName());
        SQLiteDatabase.openOrCreateDatabase(dbPath, null);
    }

}
